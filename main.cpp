#if defined(UNICODE) && !defined(_UNICODE)
    #define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
    #define UNICODE
#endif
#define PRESSED(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)

#include <tchar.h>
#include <windows.h>
#include <string>
#include <iostream>
#include <tgmath.h>
#include "level.h"

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);
using namespace std;
bool init(Level& level);
int screen_width = 1296;
int screen_height = 999;
/*  Make the class name into a global variable  */
TCHAR szClassName[ ] = _T("CodeBlocksWindowsApp");
int WINAPI WinMain (HINSTANCE hThisInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpszArgument,
                     int nCmdShow)
{
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           _T("Code::Blocks Template Windows App"),       /* Title Text */
           WS_OVERLAPPEDWINDOW, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           screen_width,                 /* The programs width */
           screen_height,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           );
    // Init
    Level level;
    if(init(level))
    {
        std::cout << "error during initialization" << std::endl;
    }

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nCmdShow);
    float fPreviousTime;
    float fCurrentTime = GetCurrentTime();
    MSG msg;
    while (true)
    {
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE ))
        {
            if(msg.message == WM_QUIT)
                break;
            /* Translate virtual-key messages into character messages */
            TranslateMessage(&messages);
            /* Send message to WindowProcedure */
            DispatchMessage(&messages);
        }
        fPreviousTime = fCurrentTime;
        fCurrentTime = GetCurrentTime();
        //checkInput();
        float fTimeElapsed = fCurrentTime - fPreviousTime;
        if(fTimeElapsed > 0.1f)
        {
            fTimeElapsed = 0.1f;
        }
        level.checkInput(fTimeElapsed);
        level.doPositionCalculations(fTimeElapsed);
        level.draw(hwnd);
    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)                  /* handle the messages */
    {
        case WM_DESTROY:
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;
        default:                      /* for messages that we don't deal with */
            return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}

bool init(Level& level)
{
    bool return_bool = false;
    size_t nLevelWidth = 64;
    size_t nLevelHeight = 16;
    string sLevel;
    sLevel += "................................................................";
    sLevel += "................................................................";
    sLevel += "................................................................";
    sLevel += "................................................................";
    sLevel += "........ooo............ooo......................................";
    sLevel += ".......................................#.#......................";
    sLevel += ".....##########.....#########..........#.#......................";
    sLevel += "...................####.........................................";
    sLevel += "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG.##############.....########";
    sLevel += "...................................#.#...............####.......";
    sLevel += "........................############.#............####..........";
    sLevel += "........................#.....ooo....#.........####.............";
    sLevel += "........................#.############......####................";
    sLevel += "........................#....ooo.........####...................";
    sLevel += "........................###################.....................";
    sLevel += "................................................................";
    level.init(nLevelWidth, nLevelHeight, 64, 64, sLevel, Player(), string("./config/config.txt"));
    return return_bool;
}